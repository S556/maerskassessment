package com.maersk.steps;

import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;

import com.maersk.test.TestBase;
import com.maersk.utils.rest.CustomExtentReporter;
import com.maersk.utils.rest.ExecutionUtils;
import com.maersk.utils.rest.UrlResources;

import cucumber.api.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;

@SuppressWarnings("deprecation")
public class WeatherTest extends TestBase{
	Response response = null;
	private static CustomExtentReporter extenthtmlreporter;
	private static boolean isExtentRunning;
	static String apiKey = null;
	ExecutionUtils executionUtils;

	public WeatherTest() {
		new ExecutionUtils();
		apiKey = ExecutionUtils.getApiKey();
	}

	@Before
	public void isExtentRunningCheck() {
		if(!isExtentRunning) {
			extenthtmlreporter = new CustomExtentReporter(System.getProperty("user.dir")+"\\target\\extent-report.html");
			isExtentRunning=true;
		}
	}

	@Given("Get weather details for city with name (.+)")
	public void getWeatherDetails(String countryName) {
		response = request.get(UrlResources.getWeather(), "q",countryName, "appid",apiKey);
		System.out.println("Response String -" + response.getBody().asString());
	}

	@Given("Get weather details for city with id (.+)")
	public void getWeatherDetailsID(String countryId) {
		response = request.get(UrlResources.getWeather(), "id",countryId, "appid",apiKey);
		System.out.println("Response String -" + response.getBody().asString());
	}

	@Then("Validate country coordinates (.+) and (.+)")
	public void verifyCountryCoordinates(String countryLong, String countryLat) {
		Map<String, String> coordnates = new HashMap<String, String>();
		coordnates = response.jsonPath().get("coord");
		Assert.assertEquals(countryLong.trim(), String.valueOf(coordnates.get("lon")));
		Assert.assertEquals(countryLat.trim(), String.valueOf(coordnates.get("lat")));
	}

	@And("Validate country name (.+) and timezone (.+)")
	public void verifyCountryNameAndtimeZone(String cName, String cTimezone) {

		Assert.assertEquals(String.valueOf(response.jsonPath().get("timezone")), cTimezone.trim());
		Assert.assertEquals(String.valueOf(response.jsonPath().get("name")), cName.trim());
	}

	@And("Validate weather desc (.+)")
	public void verifyCountryWeatDesc(String wetherDesc) {
		boolean flag = false;
		if(response.jsonPath().get("weather.description").toString().contains(wetherDesc))
			flag = true;
		Assert.assertTrue(flag);
	}

	@After
	public void AfterScenario(@SuppressWarnings("deprecation") Scenario scenario) {
		extenthtmlreporter.createTest(scenario);
		extenthtmlreporter.writeToReport();
	}

}
