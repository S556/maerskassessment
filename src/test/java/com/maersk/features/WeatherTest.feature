Feature: WeatherTest

  Scenario Outline: Validate weather country coordinates, name, timezone, weather desc
    Given Get weather details for city with name <CITYNAME>
    Then Validate country coordinates <COORD-LONG> and <COORD-LAT>
    And Validate country name <CITYNAME> and timezone <TIMEZONE>
    And Validate weather desc <WEATHERDESC>

    Examples: 
      | CITYNAME  | COORD-LONG | COORD-LAT | TIMEZONE | WEATHERDESC |
      | London    |    -0.1257 |   51.5085 |     3600 | clouds      |
      | Chennai   |    80.2785 |   13.0878 |    19800 | clouds      |
      | Hyderabad |    78.4744 |   17.3753 |    19800 | clouds      |

  Scenario: Validate weather details based on the city id
    Given Get weather details for city with id 2172797
    Then Validate country coordinates 145.7667 and -16.9167
    And Validate country name Cairns and timezone 36000
    And Validate weather desc clouds
