package com.maersk.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = { "pretty", "html:target/reports/cucumber-html-report", "json:target/cucumber-reports/Cucumber.json"},
		features = "src/test/java/com/maersk/features",
		glue = "com.maersk.steps",
		monochrome = true,
		strict = true)

public class CucumberRunner {}
