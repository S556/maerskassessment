package com.maersk.utils.rest;


public class UrlResources {

	private static String url = ExecutionUtils.getUrl();
	public static final String WEATHER = url + "weather";

	/**
	 * 
	 * @return
	 */
	public static String getWeather() {
		return WEATHER;
	}

}
