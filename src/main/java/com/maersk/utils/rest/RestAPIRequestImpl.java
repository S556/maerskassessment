package com.maersk.utils.rest;

import io.restassured.config.DecoderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;

/*
 * @author - Sreekanth
 * 
 */

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.log4j.Logger;
import static io.restassured.RestAssured.given;

import java.util.Map;

public class RestAPIRequestImpl {
	Response response = null;

	private static Logger LOGGER = Logger.getLogger(RestAPIRequestImpl.class);

	/**
	 * Method for get call with query params
	 * @param url
	 * @param queryParamName
	 * @param queryParams
	 * @return
	 */
	public Response get(String url, String qParamName1, String qParamValue1, String qParamName2, String qParamValue2) {
		Response response = given().headers("Content-Type", "application/json")
				.queryParams(qParamName1,qParamValue1)
				.queryParams(qParamName2,qParamValue2)
				.get(url).then().extract().response();
		LOGGER.info(response.getBody().asString());
		return response;
	}


	/**
	 * Method for POST call
	 * @author Sreekanth
	 * @param json
	 * @param url
	 * @return
	 */
	public Response post(String json, String url) {
		response = given().headers("Content-Type", "application/json").contentType(ContentType.JSON).body(json)
				.post(url).then().extract().response();
		LOGGER.info(response.getBody().asString());
		return response;
	}

	/**
	 * Method for get call
	 * @author Sreekanth
	 * @param url
	 * @return
	 */
	public Response get(String url) {
		response = given().headers("Content-Type", "application/json").get(url).then().extract().response();
		LOGGER.info(response.getBody().asString());
		return response;
	}

	/**
	 * Method for get call with query params as map
	 * @param url
	 * @param queryParamName
	 * @param queryParams
	 * @return
	 */
	public Response get(String url, Map<String, String> queryParams) {
		Response response = given().headers("Content-Type", "application/json").queryParams(queryParams).get(url).then().extract().response();
		LOGGER.info(response.getBody().asString());
		return response;
	}

}
