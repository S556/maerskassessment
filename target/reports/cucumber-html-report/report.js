$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/com/maersk/features/WeatherTest.feature");
formatter.feature({
  "name": "WeatherTest",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Validate weather country coordinates, name, timezone, weather desc",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "Get weather details for city with name \u003cCITYNAME\u003e",
  "keyword": "Given "
});
formatter.step({
  "name": "Validate country coordinates \u003cCOORD-LONG\u003e and \u003cCOORD-LAT\u003e",
  "keyword": "Then "
});
formatter.step({
  "name": "Validate country name \u003cCITYNAME\u003e and timezone \u003cTIMEZONE\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "Validate weather desc \u003cWEATHERDESC\u003e",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "CITYNAME",
        "COORD-LONG",
        "COORD-LAT",
        "TIMEZONE",
        "WEATHERDESC"
      ]
    },
    {
      "cells": [
        "London",
        "-0.1257",
        "51.5085",
        "3600",
        "clouds"
      ]
    },
    {
      "cells": [
        "Chennai",
        "80.2785",
        "13.0878",
        "19800",
        "clouds"
      ]
    },
    {
      "cells": [
        "Hyderabad",
        "78.4744",
        "17.3753",
        "19800",
        "clouds"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Validate weather country coordinates, name, timezone, weather desc",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Get weather details for city with name London",
  "keyword": "Given "
});
formatter.match({
  "location": "WeatherTest.getWeatherDetails(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate country coordinates -0.1257 and 51.5085",
  "keyword": "Then "
});
formatter.match({
  "location": "WeatherTest.verifyCountryCoordinates(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate country name London and timezone 3600",
  "keyword": "And "
});
formatter.match({
  "location": "WeatherTest.verifyCountryNameAndtimeZone(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate weather desc clouds",
  "keyword": "And "
});
formatter.match({
  "location": "WeatherTest.verifyCountryWeatDesc(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Validate weather country coordinates, name, timezone, weather desc",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Get weather details for city with name Chennai",
  "keyword": "Given "
});
formatter.match({
  "location": "WeatherTest.getWeatherDetails(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate country coordinates 80.2785 and 13.0878",
  "keyword": "Then "
});
formatter.match({
  "location": "WeatherTest.verifyCountryCoordinates(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate country name Chennai and timezone 19800",
  "keyword": "And "
});
formatter.match({
  "location": "WeatherTest.verifyCountryNameAndtimeZone(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate weather desc clouds",
  "keyword": "And "
});
formatter.match({
  "location": "WeatherTest.verifyCountryWeatDesc(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Validate weather country coordinates, name, timezone, weather desc",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Get weather details for city with name Hyderabad",
  "keyword": "Given "
});
formatter.match({
  "location": "WeatherTest.getWeatherDetails(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate country coordinates 78.4744 and 17.3753",
  "keyword": "Then "
});
formatter.match({
  "location": "WeatherTest.verifyCountryCoordinates(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate country name Hyderabad and timezone 19800",
  "keyword": "And "
});
formatter.match({
  "location": "WeatherTest.verifyCountryNameAndtimeZone(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate weather desc clouds",
  "keyword": "And "
});
formatter.match({
  "location": "WeatherTest.verifyCountryWeatDesc(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Validate weather details based on the city id",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Get weather details for city with id 2172797",
  "keyword": "Given "
});
formatter.match({
  "location": "WeatherTest.getWeatherDetailsID(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate country coordinates 145.7667 and -16.9167",
  "keyword": "Then "
});
formatter.match({
  "location": "WeatherTest.verifyCountryCoordinates(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate country name Cairns and timezone 36000",
  "keyword": "And "
});
formatter.match({
  "location": "WeatherTest.verifyCountryNameAndtimeZone(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Validate weather desc clouds",
  "keyword": "And "
});
formatter.match({
  "location": "WeatherTest.verifyCountryWeatDesc(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});